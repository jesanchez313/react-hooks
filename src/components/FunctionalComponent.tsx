import React, { useContext } from "react";
import { Theme } from "global/Theme";
import { Language } from "global/Language";
import { useDocumentTitle, useWindowResize, useInputValue } from "hooks/Custom";

const FunctionalComponent: React.FC = () => {
  const name = useInputValue("Jeferson");
  const surname = useInputValue("Sanchez");
  const theme = useContext(Theme);
  const lan = useContext(Language);
  const width = useWindowResize();
  useDocumentTitle(`${name.value} ${surname.value}`);

  return (
    <div className={theme}>
      <div className="form-group">
        <label htmlFor="name">Name:</label>
        <input type="text" className="form-control" id="name" {...name} />
      </div>
      <div className="form-group">
        <label htmlFor="name">Surname:</label>
        <input type="text" className="form-control" id="name" {...surname} />
      </div>
      <div className="text-center">
        <span>{lan}</span>
      </div>
      <div className="text-center">
        <span>{width}</span>
      </div>
    </div>
  );
};

export default FunctionalComponent;
