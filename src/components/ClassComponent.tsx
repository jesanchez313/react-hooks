import React from "react";
import { IComponent } from "config/interfaces";
import { Theme } from "global/Theme";
import { Language } from "global/Language";

export default class ClassComponent extends React.Component<{}, IComponent> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: "Jeferson",
      surname: "Sanchez",
      width: window.innerWidth
    };
  }

  componentDidMount() {
    document.title = `${this.state.name} ${this.state.surname}`;
    window.addEventListener("resize", this.handleResize);
  }

  componentDidUpdate() {
    document.title = `${this.state.name} ${this.state.surname}`;
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  handleResize = () => {
    this.setState({
      width: window.innerWidth
    });
  };

  handleInputName = (e: any) => {
    this.setState({
      name: e.target.value
    });
  };

  handleInputSurname = (e: any) => {
    this.setState({
      surname: e.target.value
    });
  };

  render() {
    return (
      <Theme.Consumer>
        {theme => (
          <div className={theme}>
            <div className="form-group">
              <label htmlFor="name">Name:</label>
              <input
                type="text"
                className="form-control"
                id="name"
                value={this.state.name}
                onChange={this.handleInputName}
              />
            </div>
            <div className="form-group">
              <label htmlFor="name">Surname:</label>
              <input
                type="text"
                className="form-control"
                id="name"
                value={this.state.surname}
                onChange={this.handleInputSurname}
              />
            </div>
            <Language.Consumer>
              {lan => (
                <div className="text-center">
                  <span>{lan}</span>
                </div>
              )}
            </Language.Consumer>
            <div className="text-center">{this.state.width}</div>
          </div>
        )}
      </Theme.Consumer>
    );
  }
}
