import { useEffect, useState } from "react";

export const useDocumentTitle = (title: string): void => {
  useEffect(() => {
    document.title = title;
  });
};

export const useWindowResize = (): number => {
  const [width, setWidth] = useState(window.innerWidth);

  const handleResize = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  return width;
};

export const useInputValue = (initialState: string) => {
  const [value, setValue] = useState(initialState);
  const handleInput = (e: any) => {
    setValue(e.target.value);
  };

  return {
    value,
    onChange: handleInput
  };
};
