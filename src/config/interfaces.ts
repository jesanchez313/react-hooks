export interface IComponent {
  name: string;
  surname: string;
  width: number;
}
