import React from "react";
import ClassComponent from "components/ClassComponent";
import FunctionalComponent from "components/FunctionalComponent";

const App: React.FC = () => {
  return (
    <div className="container">
      <div className="row vh-100">
        <div className="col-12 col-class d-flex justify-content-center align-items-center">
          <ClassComponent></ClassComponent>
        </div>
        <div className="col-12 col-functional d-flex justify-content-center align-items-center">
          <FunctionalComponent></FunctionalComponent>
        </div>
      </div>
    </div>
  );
};

export default App;
